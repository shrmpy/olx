// playlist tool on Netlify hosting
package main

import (
	"context"
	//"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/alexedwards/scs/v2"
	"github.com/apex/gateway"
	pldl "gitlab.com/shrmpy/olx/catalog"
	mw "gitlab.com/shrmpy/olx/middleware"
)

// composite of server fields that's unecessary in catalog namespace
type lmbconf struct {
	offline  bool
	port     int
	sessions *scs.SessionManager
	pldl.Config
}

var sessionManager *scs.SessionManager

func main() {
	ctx := context.Background()
	// Initialize a new session manager and configure the session lifetime.
	sessionManager = scs.New()
	sessionManager.Lifetime = 2 * time.Hour
	mux := http.NewServeMux()
	conf := readArgs(sessionManager)

	// unless specified, use AWS server
	listener := gateway.ListenAndServe
	host := "n/a"

	if conf.offline {
		// use local (dev) HTTP server
		host = fmt.Sprintf(":%d", conf.port)
		listener = http.ListenAndServe
		mux.Handle("/", http.FileServer(http.Dir("./public")))
	}

	// TODO changing instantiator also means config isn't parameter here
	conf.Network = "twitch" //only twitch for now //network(r.URL.Path)
	b := ctob(conf)
	cl := pldl.NewCataloger(b)

	err := cl.PreOIDC(ctx)
	if err != nil {
		log.Fatal(err)
	}

	mux.HandleFunc("/api/twitch/", mw.MiddlewareAuth(cl.Playlist, cl))
	mux.HandleFunc("/auth/twitch/callback/", cl.HandleOAuth2Callback)

	log.Printf("listening on http://%s/", host)
	log.Fatal(listener(host, sessionManager.LoadAndSave(mux)))
}

func readArgs(sessions *scs.SessionManager) *lmbconf {
	var conf lmbconf

	flag.StringVar(&conf.ClientID, pldl.CLI_CID, "", "Client ID of registered app")
	flag.DurationVar(&conf.Timeout, pldl.CLI_TMO, 120*time.Second, "URL open timeout")
	flag.StringVar(&conf.HelixID, pldl.CLI_HID, "", "Helix ID for new API")
	flag.StringVar(&conf.HelixSecret, pldl.CLI_SEC, "", "Helix secret of registered app")
	flag.StringVar(&conf.HelixRedir, pldl.CLI_RED, "", "the redirect URI must be exact match of the registered field in the Twitch API console")

	// port is specific to server
	flag.IntVar(&conf.port, "port", 8000, "port to bind for httpd")
	flag.BoolVar(&conf.offline, "offline", false, "use local HTTP instead of AWS lambda for debugging")

	flag.Parse()
	conf.sessions = sessions
	return &conf
}

// shape fields to the common config
func ctob(c *lmbconf) *pldl.Config {
	return &pldl.Config{
		ClientID:    c.ClientID,
		HelixID:     c.HelixID,
		HelixSecret: c.HelixSecret,
		HelixRedir:  c.HelixRedir,
		Timeout:     c.Timeout,
		Network:     c.Network,
		Sessions:    c.sessions,
	}
}
