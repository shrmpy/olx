module gitlab.com/shrmpy/olx

go 1.15

require (
	github.com/alexedwards/scs/v2 v2.4.0
	github.com/apex/gateway v1.1.1
	github.com/aws/aws-lambda-go v1.19.1 // indirect
	github.com/coreos/go-oidc v2.2.1+incompatible
	github.com/grafov/m3u8 v0.11.1
	github.com/pkg/errors v0.9.1
	github.com/pquerna/cachecontrol v0.0.0-20200921180117-858c6e7e6b7e // indirect
	github.com/tj/assert v0.0.3 // indirect
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43
	gopkg.in/square/go-jose.v2 v2.5.1 // indirect
)
