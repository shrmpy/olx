package middleware

import (
	"context"
	//"fmt"
	//"net/http"
)

// see context propogation
// https://medium.com/@rakyll/context-propagation-over-http-in-go-d4540996e9b0

// WithHelixToken puts the request token into the current context.
func withHelixToken(ctx context.Context, token string) context.Context {
	return context.WithValue(ctx, contextHelixTokenKey, token)
}
func WithSearchKeyword(ctx context.Context, keyword string) context.Context {
	return context.WithValue(ctx, contextSearchKeywordKey, keyword)
}

// HelixTokenFromContext returns the request token from the context.
// A zero ID is returned if there are no idenfiers in the
// current context.
func helixTokenFromContext(ctx context.Context) string {
	v := ctx.Value(contextHelixTokenKey)
	if v == nil {
		return ""
	}
	return v.(string)
}
func SearchKeywordFromContext(ctx context.Context) string {
	v := ctx.Value(contextSearchKeywordKey)
	if v == nil {
		return ""
	}
	return v.(string)
}

type contextHelixTokenType struct{}
type contextSearchKeywordType struct{}

var contextHelixTokenKey = &contextHelixTokenType{}
var contextSearchKeywordKey = &contextSearchKeywordType{}

const HelixTokenHeader = "Helix-Token"
const SearchKeywordHeader = "Search-Keyword"

/*
// Transport serializes the request context into request headers.
type Transport struct {
	// Base is the actual round tripper that makes the request.
	// http.DefaultTransport is used if none is set.
	Base http.RoundTripper
}
// RoundTrip converts request context into headers
// and makes the request.
func (t *Transport) RoundTrip(r *http.Request) (*http.Response, error) {
	r = cloneReq(r) // per RoundTrip interface enforces
	rid := request.SearchKeywordFromContext(r.Context())
	if rid != "" {
		r.Header.Add(searchKeywordHeader, rid)
	}
	base := t.Base
	if base == nil {
		base = http.DefaultTransport
	}
	return base.RoundTrip(r)
}

// Handler deserializes the request context from request headers.
type Handler struct {
	// Base is the actual handler to call once deserialization
	// of the context is done.
	Base http.Handler
}
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	rid := r.Header.Get(searchKeywordHeader)
	if rid != "" {
		r = r.WithContext(request.WithSearchKeyword(r.Context(), rid))
	}
	h.Base.ServeHTTP(w, r)
}

*/
