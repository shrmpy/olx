// middleware contains handler logic to perform authorization
package middleware

import (
	"net/http"
	//"net/url"
	"regexp"

	pldl "gitlab.com/shrmpy/olx/catalog"
)

var (
	RE_API_GAME = regexp.MustCompile(`/api/twitch/games/(?P<id>[^/#?]+)`)
)

// middleware to check authorization is satisfied
func MiddlewareAuth(fn func(http.ResponseWriter, *http.Request), cl pldl.Catalog) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Limit to 5 mins between cache replenish
		w.Header().Set("Cache-Control", "public, max-age=300")

		// check whether authorization is required
		if IsOIDC(r.URL.Path) {
			// need to call vendor-specific (Twitch) API
			tw := cl.(*pldl.Twitch)

			sesskey := r.URL.Query().Get("debug")
			scx, _ := tw.Sessions.Load(r.Context(), sesskey)
			token := tw.Sessions.GetString(scx, HelixTokenHeader)

			if token == "" {
				// obtain URL prompting the user to visit Twitch auth sign-in
				auth := tw.AuthCodeURL()
				w.Write([]byte(auth))

				// short-circuit to require auth sign-in
				// TODO to reconstitute this state after the redirect,
				// marshal input fields on the form into cookies/context
				// headers (which will get returned in the redirect?)
				return
			}
		}

		// safe/authorized to continue to next handler
		fn(w, r)
	}
}

// check whether the request requires authorization
func IsOIDC(path string) bool {
	return RE_API_GAME.MatchString(path)
}
