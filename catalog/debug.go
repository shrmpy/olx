package catalog

import (
	"fmt"
	"reflect"
)

// helper to print info
func Debug(thing interface{}) {
	var st reflect.Type
	to := reflect.TypeOf(thing)

	kind := to.Kind()
	if kind == reflect.Ptr {
		st = to.Elem()
		kind = st.Kind()
	} else {
		st = to
	}

	name := st.Name()

	fmt.Printf("DEBUG thing is named: %s \n", name)
	fmt.Printf("DEBUG %s is from %s \n", name, st.PkgPath())
	fmt.Printf("DEBUG %s is a - %s \n", name, kind)
	if kind == reflect.Struct {
		fmt.Printf("DEBUG %s has %d methods \n", name, st.NumMethod())
		fmt.Printf("DEBUG %s has %d fields \n", name, st.NumField())
	}
	fmt.Printf("DEBUG - %s \n", st.String())
}
