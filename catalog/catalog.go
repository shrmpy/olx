// Package catalog is a playlist interface for the different networks
package catalog

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

type Catalog interface {
	Network() string
	Collection(u *url.URL) []Media
	Playlist(w http.ResponseWriter, r *http.Request)
	PreOIDC(ctx context.Context) error
	HandleOAuth2Callback(http.ResponseWriter, *http.Request)
}

type Media struct {
	Title   string `json:"title"`
	URL     string `json:"url"`
	Quality string `json:"quality"`
}

// creates instances of "catalogers"
func NewCataloger(conf *Config) Catalog {
	// TODO should we keep map of instances because getting
	// invoked from playlistHandler repeats the instantiations here

	switch conf.Network {
	case "youtube":
		return NewYoutuber()

	case "twitch":
		return NewTwitcher(conf)

	default:
		return NullCataloger()
	}
}

// serialize queue to JSON
func Marshal(q []Media) []byte {
	dat := make(map[string][]Media)
	dat["items"] = q

	buf, err := json.Marshal(dat)

	if err != nil {
		fmt.Printf("DEBUG marshal error: %v \n", err)
		// TODO capture error
		return []byte{}
	}

	return buf
}

// Null/zero implementation
type NullCatalog struct{}

func NullCataloger() Catalog           { return &NullCatalog{} }
func (c *NullCatalog) Network() string { return "" }

func (c *NullCatalog) Collection(u *url.URL) []Media                               { return []Media{} }
func (c *NullCatalog) Playlist(w http.ResponseWriter, r *http.Request)             {}
func (c *NullCatalog) HandleOAuth2Callback(w http.ResponseWriter, r *http.Request) {}
func (c *NullCatalog) PreOIDC(ctx context.Context) error                           { return nil }
