package catalog

import (
	"context"
	"net/http"
	"net/url"
)

type Youtube struct {
	network string
}

func NewYoutuber() Catalog {
	return &Youtube{network: "youtube"}
}

func (c *Youtube) Network() string {
	return c.network
}

func (c *Youtube) Collection(u *url.URL) []Media                               { return []Media{} }
func (c *Youtube) Playlist(w http.ResponseWriter, r *http.Request)             {}
func (c *Youtube) HandleOAuth2Callback(w http.ResponseWriter, r *http.Request) {}
func (c *Youtube) PreOIDC(ctx context.Context) error                           { return nil }
